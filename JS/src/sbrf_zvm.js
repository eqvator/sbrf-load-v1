const puppeteer = require('puppeteer');
const path = require('path');

if (process.argv.length !== 5) {
    console.log('error args')
    return
}

const pin = process.argv[2]
const login = process.argv[3]
const pass = process.argv[4];

const delay = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms))
}

const script = {
    url_login: "http://localhost:28016/vpnkeylocal/#/ANY_API/login/",
    url_logout: "http://localhost:28016/vpnkeylocal/logout/",
    browser: null,
    page: null,
    methods: {
        async init_browser() {
            try {
                script.browser = await puppeteer.launch({
                    //executablePath: path.join(__dirname, "chrome-win\\chrome.exe"),
                    headless: false,
                    defaultViewport: {
                        width: 1920, height: 1080
                    },
                    args: ['--start-maximized']
                });
            } catch (error) {
                console.log('init_browser error')
                throw error
            }
        },
        async init_page() {
            try {
                script.page = await script.browser.newPage();
            } catch (error) {
                throw error
            }
        },
        async open_logout_page() {
            await script.page.goto(script.url_logout);
        },
        async wait_login_PIN() {
            // await script.page.waitForSelector('ul[class~="nav_login"]', {timeout: 60000})
            //     .then(() => {
            //         console.log("Step ENTER 1")
            //     })
            //     .catch(err => {
            //         throw err
            //     });
            //
            // await script.page.waitForSelector('ul[class~="nav_login"] > li > a', {timeout: 30000})
            //     .then(() => {
            //         console.log("Step ENTER 2")
            //     })
            //     .catch(err => {
            //         throw err
            //     })

            await script.page.waitForSelector('input[type="password"]', {timeout: 30000})
                .then(() => {
                    console.log("Step ENTER 3")
                })
                .catch(err => {
                    throw err
                })
        },
        async enter_with_pin() {
            await delay(1000)
            await script.page.type('input[type="password"]', pin, {delay: 20});
            await delay(500)
            await script.page.click('button[type="submit"]')
            console.log()
        },
        async enter_SSBOL() {
            await script.page.waitForSelector('#btn-business-system-card-v-0', {timeout: 60000})
                .then(() => {
                    console.log("Step START ENTER PAGE SSBOL")
                })
                .catch(err => {
                    throw err
                })

            await script.page.click('#btn-business-system-card-v-0')
            console.log("CLICK SSBOL")
            await delay(1000)
        },
        async fill_SSBOL_auth() {
            await script.page.waitForSelector('#inputLogin', {timeout: 60000})
                .then(() => {
                    console.log("Step FILL AUTH SSBOL - have login input")
                })
                .catch(err => {
                    throw err
                })

            await script.page.waitForSelector('#inputPassword', {timeout: 60000})
                .then(() => {
                    console.log("Step FILL AUTH SSBOL - have pass input")
                })
                .catch(err => {
                    throw err
                })

            await script.page.type('#inputLogin', login, {delay: 20}); // Types slower, like a user
            await script.page.type('#inputPassword', pass, {delay: 20}); // Types slower, like a user
            await script.page.$eval('[data-test-id="LoginForm__submit--button"]', el => el.click());
            await delay(1000)
        },
        async open_statements(){
            await script.page.waitForSelector('button[data-tutorial-id="tutorial-statementsMenu"]', {timeout: 60000})
                .then(() => {
                    console.log("Step STATEMENTS")
                })
                .catch(err => {
                    throw err
                })

            await page.$eval('button[data-tutorial-id="tutorial-statementsMenu"]', el => el.click());
            await delay(1000)
            await page.$eval('button[data-tutorial-id="tutorial-statementsMenu"]', el => el.click());
            await delay(1000)


        },
        async create_statements(){
            await script.page.waitForSelector('button[data-tutorial-id="tutorial-createStatement"]', {timeout: 60000})
                .then(() => {
                    console.log("Step CREATE STATEMENTS")
                })
                .catch(err => {
                    throw err
                })

            await page.$eval('button[data-tutorial-id="tutorial-createStatement"]', el => el.click());

            await script.page.waitForSelector('button[data-analytics-label="DOWNLOAD_FILE"]', {timeout: 60000})
                .then(() => {
                    console.log("Step DOWNLOAD STATEMENTS")
                })
                .catch(err => {
                    throw err
                })
        },
    }

}
const start = async () => {
    try {
        console.log("START")
        await script.methods.init_browser()
        await script.methods.init_page()
        await script.methods.open_logout_page()
        await script.methods.wait_login_PIN()
        await script.methods.enter_with_pin()
        await script.methods.enter_SSBOL()
        await script.methods.fill_SSBOL_auth()
        console.log("END OK")
        await delay(5000)
        //script.page.close()
    } catch (e) {
        // script.browser.close()
        console.log('ERROR', e.message)
        console.log("END BAD")
        await delay(15000)
        return
    }
}
start()
