#include <Constants.au3>
#include <WinAPISys.au3>

FileChangeDir(@ScriptDir)
Global $path_to_ini = "sbrf_zvm.ini"
Global $new_ini = "sbrf_zvm_copy.ini"

Global $pin 	= ""
Global $login 	= ""
Global $pass 	= ""

Global $path_to_node
Global $path_to_js_script
Global $disk_to_start
Global $path_to_start

Global $path_from_move
Global $path_to_move

CreateInitFile()

CheckParams()
FillParams()

CheckIniFile()
FillVars()
DeleteFileFrom()

CloseAllStartExe()
CloseAllIE()

CloseExplorer()

RunStartExe()
CloseAllIE()

RunJSScript()

FileResultMove()

CloseAllStartExe()

Exit

#Region Functions

Func CreateInitFile()

   If $CmdLine[0] = 1 And $CmdLine[1] = "init" Then
	  IniWrite($new_ini, "CONFIG", "path_to_node", "C:\Program Files\nodejs\node.exe")
	  IniWrite($new_ini, "CONFIG", "disk_to_start", "D:\")
	  IniWrite($new_ini, "CONFIG", "path_to_start", "D:\start.exe")
	  IniWrite($new_ini, "CONFIG", "path_to_js_script", "C:\autoIt\sbrf_zvm.js")

	  IniWrite($new_ini, "CONFIG", "path_from_move", 	"C:\Users\Монитор\Downloads\kl_to_1c.txt")
	  IniWrite($new_ini, "CONFIG", "path_to_move", 		"C:\autoIt\1C\sbrf_zvm_kl_to_1c.txt")
	  Exit
   EndIf

EndFunc

Func CheckParams()

   If $CmdLine[0] <> 3 Then
	  Exit 1001
   EndIf

EndFunc

Func FillParams()
   $pin 	= $CmdLine[1]
   $login 	= $CmdLine[2]
   $pass 	= $CmdLine[3]

   If $pin = "" Or $login = "" Or $pass = "" Then
	  Exit
   EndIf
EndFunc

Func CheckIniFile()
   If FileExists($path_to_ini) <> 1 Then
	  MsgBox($MB_SYSTEMMODAL + $MB_ICONERROR, "CONFIG Error", "sbrf.ini not exist")
	  Exit
   EndIf
EndFunc

Func FillVars()

   $path_to_node 		= IniRead($path_to_ini, "CONFIG", "path_to_node", 		"C:\Program Files\nodejs\node.exe")
   $disk_to_start 		= IniRead($path_to_ini, "CONFIG", "disk_to_start", 		"E:\")
   $path_to_start 		= IniRead($path_to_ini, "CONFIG", "path_to_start", 		"E:\start.exe")
   $path_to_js_script 	= IniRead($path_to_ini, "CONFIG", "path_to_js_script", 	"sbrf.js")

   $path_from_move 		= IniRead($path_to_ini, "CONFIG", "path_from_move", "")
   $path_to_move 		= IniRead($path_to_ini, "CONFIG", "path_to_move", 	"")

   If FileExists($path_to_node) <> 1 Then
	  MsgBox($MB_SYSTEMMODAL + $MB_ICONERROR, "CONFIG Error", "node.exe not exist.")
	  Exit
   EndIf

   If FileExists($path_to_start) <> 1 Then
	  MsgBox($MB_SYSTEMMODAL + $MB_ICONERROR, "CONFIG Error", "start.exe not exist.")
	  Exit
   EndIf

   If FileExists($path_to_js_script) <> 1 Then
	  MsgBox($MB_SYSTEMMODAL + $MB_ICONERROR, "CONFIG Error", "js script not exist.")
	  Exit
   EndIf

   If $path_from_move = "" Then
	  MsgBox($MB_SYSTEMMODAL + $MB_ICONERROR, "CONFIG Error", "path from move is empty.")
	  Exit
   EndIf

   If $path_to_move = "" Then
	  MsgBox($MB_SYSTEMMODAL + $MB_ICONERROR, "CONFIG Error", "path to move is empty.")
	  Exit
   EndIf

EndFunc

Func CloseAllStartExe()
   Local $aProcessList = ProcessList("start.exe")
   Local $total_process = $aProcessList[0][0]

   For $i = 1 to $total_process
	  ProcessClose($aProcessList[$i][1])
   Next

   Sleep(300)
EndFunc

Func CloseAllIE()
   Local $aProcessList = ProcessList("iexplore.exe")
   Local $total_process = $aProcessList[0][0]

   For $i = 1 to $total_process
	  ProcessClose($aProcessList[$i][1])
   Next

   Sleep(300)
EndFunc

Func CloseExplorer()
   While WinExists("[CLASS:CabinetWClass]")
	  WinClose("[CLASS:CabinetWClass]")
   WEnd
EndFunc

Func RunStartExe()
   Run($path_to_start, "",@SW_MAXIMIZE)
   Sleep(1000)
EndFunc

Func RunJSScript()
   Local $cmd = $path_to_node & " " & $path_to_js_script & " " & $pin & " " & $login & " " & $pass
   RunWait($cmd, "", @SW_MAXIMIZE)
EndFunc

Func DeleteFileFrom()
   If FileExists($path_from_move) = 1 Then
		 FileDelete($path_from_move)
   EndIf
EndFunc

Func FileResultMove()

   For $i = 5 To 1 Step -1
	  If FileExists($path_from_move) <> 1 Then
		 Sleep(1000)
	  EndIf
	  FileMove($path_from_move, $path_to_move, $FC_OVERWRITE)
	  ExitLoop
   Next

EndFunc

#EndRegion